var counter = 0, // to keep track of current slide
    $items = $('.diy-slideshow figure'), // a collection of all of the slides, caching for performance
    numItems = $items.length; // total number of slides

// this function is what cycles the slides, showing the next or previous slide and hiding all the others
var showCurrent = function(){
    var itemToShow = Math.abs(counter%numItems);// uses remainder (aka modulo) operator to get the actual index of the element to show

  $items.removeClass('show'); // remove .show from whichever element currently has it
  $items.eq(itemToShow).addClass('show');
};

// add click events to prev & next buttons
$('.next').on('click', function(){
    counter++;
    showCurrent();
});
$('.prev').on('click', function(){
    counter--;
    showCurrent();
});

//burger menu

$('#menu_head').click(function(){
      $(document).scrollTop($("header").offset().top );
     $('.menu_head').toggleClass("open");
     if ($('.menu_head').hasClass("open")) {
        var x=window.scrollX;
        var y=window.scrollY;
        window.onscroll=function(){window.scrollTo(x, y);};
     } else {
        window.onscroll=function(){};
     }
});

$('.hvr').click(function(){
    $('.menu_head').toggleClass("open");
   window.onscroll=function(){};
});

// if touch events are supported then add swipe interactions using TouchSwipe https://github.com/mattbryson/TouchSwipe-Jquery-Plugin
if('ontouchstart' in window){
  $('.diy-slideshow').swipe({
    swipeLeft:function() {
      counter++;
      showCurrent();
    },
    swipeRight:function() {
      counter--;
      showCurrent();
    }
  });
}
